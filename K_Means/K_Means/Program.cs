﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace K_Means
{
    class Program
    {
        const string IMG_PATH = @"C:\Users\joenice\Desktop\test2.jpg";
        static int K = 1;
        static bool CHANGE = true;//終止條件 不在變動

        static void Main(string[] args)
        {
            string[] fileName = new string[10] { "test1.jpg", "test2.jpg", "test3.jpg", "test4.jpg", "test5.jpg", "test6.jpg", "test7.jpg", "test8.jpg", "test9.jpg" , "test10.jpg"};

            while (K <= 10)
            {
                //取得RGB(x,y,rgbk)
                double[][][] pixel = GetPixel();

                //在訓練組資料中「隨機」找出K筆紀錄來作為初始種子(初始群集的中心) ;center:[0~(k-1)][rgb]
                double[][] center = RadomK(pixel);

                //產生初始群集
                //計算每一筆紀錄到各個隨機種子之間的距離，
                //然後比較該筆紀錄究竟離哪一個隨機種子最近，
                //然後這筆紀錄就會被指派到最接近的那個群集中心，
                //此時就會形成一個群集邊界，產生了初始群集的成員集合
                //mean[][x,y]
                pixel = InitClustering(pixel, center,true);

                //有變更就繼續執行
                int i = 0;
                while (CHANGE)
                {
                    //根據邊界內的每一個案例重新計算出該群集的質量中心，利用新的質量中心取代之前的隨機種子，來做為該群的中心
                    center = GetNewCenter(pixel);

                    //指定完新的質量中心之後，再一次比較每一筆紀錄與新的群集中心之間的距離，然後根據距離，再度重新分配每一個案例所屬的群集
                    pixel = InitClustering(pixel, center,false);
                    i++;
                }

                //int[] group = new int[K];
                //for (int g = 0; g < pixel.Length; g++)
                //{
                //    for (int j = 0; j < pixel[0].Length; j++)//每筆紀錄
                //    {
                //        group[(int)pixel[g][j][3]]++;
                //    }
                //}

                CreateImage(pixel, center, fileName[K-1]);
                K++;
            }
            Console.ReadKey();
        }


        /// <summary>
        /// 將每個點設定預設群組
        /// </summary>
        private static double[][][] InitClustering(double[][][] pixel, double[][] center,bool changeDisable)
        {
            CHANGE = false;
            for (int i = 0; i < pixel.Length; i++)
            {
                for (int j = 0; j < pixel[0].Length; j++)//每筆紀錄
                {
                    double minDistance = double.MaxValue;
                    int newGroup = 0;
                    for (int k = 0; k < K; k++)//每個隨機種子
                    {

                        double tempDis = GetDistance(pixel[i][j], center[k]);

                        if (tempDis < minDistance)
                        {
                            //更新最短距離
                            minDistance = tempDis;

                            //更新group
                            newGroup = k;
                        }
                    }
                    if ((int)pixel[i][j][3] != newGroup)
                    {
                        pixel[i][j][3] = newGroup;
                        CHANGE = true;
                    }


                }
            }

            if (changeDisable)
            {
                CHANGE = true;
            }

            return pixel;
        }




        //在訓練組資料中「隨機」找出K筆紀錄來作為初始種子(初始群集的中心)
        private static double[][] RadomK(double[][][] pixel)
        {
            double[][] radomCenter = new double[K][];
            Random random = new Random(0);


            for (int i = 0; i < K; i++)
            {
                //每個點的rgb都不同

                int x = random.Next(0, pixel.Length);
                int y = random.Next(0, pixel[0].Length);
                radomCenter[i] = pixel[x][y];

                for (int j = 0; j <= i - 1; j++)
                {
                    bool distinct = false;
                    for (int k = 0; k < radomCenter[i].Length; k++)
                    {
                        if (radomCenter[i][k] != radomCenter[j][k])
                        {
                            distinct = true;
                        }

                    }
                    if (!distinct)
                    {
                        i = i - 1;
                    }
                }


            }

            return radomCenter;
        }


        //取得每組平均 int[幾群c][r,g,b] 重新計算每一個群集中心（常用平均值）
        public static double[][] GetNewCenter(double[][][] pixel)
        {
            //根據邊界內的每一個案例重新計算出該群集的質量中心，利用新的質量中心取代之前的隨機種子，來做為該群的中心

            double[][] sum = new double[K][];//K個群組 K個加總
            //sum init
            for (int i = 0; i < sum.Length; i++)
            {
                sum[i] = new double[4];//rgb 個數(加了幾個)
                sum[i][0] = 0;//r
                sum[i][1] = 0;//g
                sum[i][2] = 0;//b
                sum[i][3] = 0;//個數
            }
            double group;
            for (int i = 0; i < pixel.Length; i++)
            {
                for (int j = 0; j < pixel[0].Length; j++)//每筆紀錄
                {
                    //get 第幾組
                    group = pixel[i][j][3];
                    sum[(int)group][0] = sum[(int)group][0] + pixel[i][j][0];//r
                    sum[(int)group][1] = sum[(int)group][1] + pixel[i][j][1];//g
                    sum[(int)group][2] = sum[(int)group][2] + pixel[i][j][2];//b
                    sum[(int)group][3]++;//個數+1
                }
            }

            double[][] average = new double[sum.Length][];

            for (int i = 0; i < sum.Length; i++)
            {
                average[i] = new double[3];
                for (int j = 0; j < average[i].Length; j++)
                {
                    average[i][j] = sum[i][j] / sum[i][3];//rgb

                }

            }

            return average;
        }



        /// <summary>
        /// 跟平均值比較取出 距離 
        /// </summary>
        /// <param name="number">資料</param>
        /// <param name="mean">每組平均值</param>
        /// <returns></returns>
        private static double GetDistance(double[] number, double[] mean)
        {
            double sum = 0.0;
            for (int i = 0; i < number.Length - 1; ++i)
            {
                sum = sum + Math.Pow((number[i] - mean[i]), 2);
            }
            return Math.Sqrt(sum);
        }

        //儲存圖片RGB K
        static double[][][] GetPixel()
        {
            Bitmap img = new Bitmap(IMG_PATH);
            double[][][] pixel = new double[img.Width][][];

            int index = 0;
            //x
            for (int i = 0; i < img.Width; i++)
            {
                pixel[i] = new double[img.Height][];
                //y
                for (int j = 0; j < img.Height; j++)
                {
                    pixel[i][j] = new double[4];
                    Color pixelColar = img.GetPixel(i, j);
                    pixel[i][j][0] = pixelColar.R;
                    pixel[i][j][1] = pixelColar.G;
                    pixel[i][j][2] = pixelColar.B;
                    //預設為第0群
                    pixel[i][j][3] = 0;
                    index++;
                }
            }
            return pixel;

        }

        public static void CreateImage(double[][][] pixel, double[][] center, string fileName)
        {
            Bitmap image = new Bitmap(pixel.Length, pixel[0].Length);
            for (int i = 0; i < pixel.Length; i++)
            {
                for (int j = 0; j < pixel[0].Length; j++)//每筆紀錄
                {
                    int group = (int)pixel[i][j][3];
                    Color color = Color.FromArgb((int)center[group][0], (int)center[group][1], (int)center[group][2]);
                    image.SetPixel(i, j, color);
                }
            }
            image.Save(string.Format(@"C:\Users\joenice\Desktop\image\{0}", fileName), ImageFormat.Jpeg);
        }
    }
}
